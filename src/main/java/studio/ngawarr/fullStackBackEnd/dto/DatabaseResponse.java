package studio.ngawarr.fullStackBackEnd.dto;

import javax.persistence.*;

@Entity
@Table(name = "test_api_table")
public class DatabaseResponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
