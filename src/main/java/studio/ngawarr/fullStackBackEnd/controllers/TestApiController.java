package studio.ngawarr.fullStackBackEnd.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import studio.ngawarr.fullStackBackEnd.TestApi;
import studio.ngawarr.fullStackBackEnd.dto.DatabaseResponse;
import studio.ngawarr.fullStackBackEnd.services.TestApiService;

import java.util.List;

@RestController
public class TestApiController {
    private static final String responseContent = "This is from my gradle springboot backend api :O";

    @Autowired
    private TestApiService testApiService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/test-api")
    public TestApi testApi() {
        System.out.println("We got a request");
        return new TestApi(responseContent);
    }

    @DeleteMapping("/delete-data")
    public void deleteData(@RequestParam Long id) {
        testApiService.deleteData(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/add-data")
    public void addData(@RequestParam String name) {
        testApiService.addData(name);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PutMapping("/update-data")
    public void updateData(@RequestParam("id") Long id, @RequestParam String name) {
        testApiService.updateData(id, name);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/get-all-data")
    public List<DatabaseResponse> getAllData() {
        List<DatabaseResponse> cool = testApiService.getData();
        return cool;
    }

}
