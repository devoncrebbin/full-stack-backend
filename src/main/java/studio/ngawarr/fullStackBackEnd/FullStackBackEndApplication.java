package studio.ngawarr.fullStackBackEnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "studio.ngawarr.fullStackBackEnd")
public class FullStackBackEndApplication {
	public static void main(String[] args) {

		SpringApplication.run(FullStackBackEndApplication.class, args);
	}

}
