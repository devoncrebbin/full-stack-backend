package studio.ngawarr.fullStackBackEnd;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import studio.ngawarr.fullStackBackEnd.dto.DatabaseResponse;

@Repository
public interface TestRepository extends CrudRepository<DatabaseResponse, Long> {
}