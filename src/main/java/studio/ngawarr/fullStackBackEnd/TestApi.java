package studio.ngawarr.fullStackBackEnd;

public class TestApi {
    private final String content;

    public TestApi(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
