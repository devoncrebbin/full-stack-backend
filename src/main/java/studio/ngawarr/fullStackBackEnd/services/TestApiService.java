package studio.ngawarr.fullStackBackEnd.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import studio.ngawarr.fullStackBackEnd.TestRepository;
import studio.ngawarr.fullStackBackEnd.dto.DatabaseResponse;

import javax.xml.crypto.Data;
import java.util.List;
import java.util.Optional;

@Service
public class TestApiService {

    @Autowired
    private TestRepository testRepository;

    public void addData(String name) {
        DatabaseResponse newResponse = new DatabaseResponse();
        newResponse.setName(name);
        testRepository.save(newResponse);
    }

    public void deleteData(Long id) {
        if (testRepository.existsById(id)) {
            testRepository.deleteById(id);
        }
    }

    public void updateData(Long id, String name) {
        if (testRepository.findById(id).isPresent()) {
            DatabaseResponse newResponse = testRepository.findById(id).get();
            newResponse.setName(name);
            testRepository.save(newResponse);
        }
    }

    public List<DatabaseResponse> getData() {
        List<DatabaseResponse> cool = (List<DatabaseResponse>) testRepository.findAll();
        return cool;
    }

}
