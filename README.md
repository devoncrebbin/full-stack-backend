# Getting Started

### Reference Documentation

**This project is running:**
<br>
*jdk1.8.0_271*
<br>
*gradle-6.7.1*

#### STEP 1
- Install the latest version of
 [PostgreSQL](https://www.postgresql.org/download/)
 
- Install [JDK 1.8](https://www.oracle.com/au/java/technologies/javase/javase-jdk8-downloads.html)
 
- Install the latest version of [pgAdmin](https://www.pgadmin.org/download/)

- Install the latest version of [Gradle](https://gradle.org/install/)

#### STEP 2

- Open up pgAdmin

- Create a database with the name **TestDatabase**

- Right click > Restore > copy database.sql path (from the root of this project) > Restore 

- Make sure the **application.properties** settings match up with your postgres settings

#### STEP 3

- Open up a terminal for this project

- Run *gradle bootrun*

- The server should be working now 🤠
